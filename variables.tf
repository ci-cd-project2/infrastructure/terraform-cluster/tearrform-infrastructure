variable "project_name" {
    type = string
    default = "final_proj"
}
variable "location" {
    type = string
    default = "westeurope"
}
 variable "db_password" {
   type = string 
   description = "password to access database"
   sensitive   = true
}


